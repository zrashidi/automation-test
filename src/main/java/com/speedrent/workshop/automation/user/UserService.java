package com.speedrent.workshop.automation.user;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Rashidi Zin
 */
@Service
@Transactional
class UserService {

    private final UserRepository repository;

    public UserService(final UserRepository repository) {
        this.repository = repository;
    }

    public User create(User user) {
        boolean usernameIsUnavailable = repository.existsByUsername(user.username());

        if (usernameIsUnavailable) {
            throw new EntityExistsException(String.format("username %s is unavailable", user.username()));
        }

        return repository.save(user);
    }

    public void delete(Long id) {
        User persisted = repository.findById(id).orElseThrow(EntityNotFoundException::new);

        repository.delete(persisted);
    }

}
