package com.speedrent.workshop.automation.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author Rashidi Zin
 */
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String name;

    @NotNull
    @Column(unique = true)
    private String username;

    public Long id() {
        return id;
    }

    public User setId(final Long id) {
        this.id = id;
        return this;
    }

    public String name() {
        return name;
    }

    public User setName(final String name) {
        this.name = name;
        return this;
    }

    public String username() {
        return username;
    }

    public User setUsername(final String username) {
        this.username = username;
        return this;
    }

}
