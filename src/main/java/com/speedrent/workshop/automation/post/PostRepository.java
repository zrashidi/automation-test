package com.speedrent.workshop.automation.post;

import java.util.Optional;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author Rashidi Zin
 */
@NoRepositoryBean
public interface PostRepository {

    Optional<Post> findById(Long id);

}
