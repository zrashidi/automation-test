package com.speedrent.workshop.automation.post;

/**
 * @author Rashidi Zin
 */
public class Post {

    private Long id;

    private String title;

    private String content;

    public Post() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

}
