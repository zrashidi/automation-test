package com.speedrent.workshop.automation.post;

import java.util.Optional;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

/**
 * @author Rashidi Zin
 */
public class PostRemoteRepository implements PostRepository {

    private final RestTemplate restTemplate;

    public PostRemoteRepository(RestTemplateBuilder builder) {
        restTemplate = builder.build();
    }

    @Override
    public Optional<Post> findById(final Long id) {
        return Optional.ofNullable(restTemplate.getForEntity("https://example.com/posts/{id}", Post.class, id).getBody());
    }

}
