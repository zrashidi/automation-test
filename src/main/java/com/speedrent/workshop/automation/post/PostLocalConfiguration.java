package com.speedrent.workshop.automation.post;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author Rashidi Zin
 */
@Configuration
@Profile("dev")
public class PostLocalConfiguration {

    @Bean
    public PostRepository postLocalRepository() {
        return new PostLocalRepository();
    }

}
