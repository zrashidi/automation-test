package com.speedrent.workshop.automation.post;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author Rashidi Zin
 */
@Configuration
@Profile("prod")
public class PostRemoteConfiguration {

    @Bean
    public PostRepository postRemoteRepository(RestTemplateBuilder builder) {
        return new PostRemoteRepository(builder);
    }

}
