package com.speedrent.workshop.automation.post;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.notFound;

/**
 * @author Rashidi Zin
 */
@RestController
public class PostController {

    private final PostRepository repository;

    public PostController(final PostRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/posts/{id}")
    public ResponseEntity<Post> findById(@PathVariable Long id) {
        return repository.findById(id).map(ResponseEntity::ok).orElse(notFound().build());
    }

}
