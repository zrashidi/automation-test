package com.speedrent.workshop.automation.post;

import java.util.Optional;

/**
 * @author Rashidi Zin
 */
public class PostLocalRepository implements PostRepository {

    @Override
    public Optional<Post> findById(final Long id) {
        Post post = new Post();

        post.setId(1531L);
        post.setTitle("Demo Post");
        post.setContent("A content that has nothing to do with the title.");

        return Optional.of(post);
    }

}
