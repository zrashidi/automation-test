# SPEEDHOME Workshop: Writing Tests With Spring Boot

## Prerequisite:
- [Java 8](https://adoptium.net/temurin/releases)
- [Docker](https://docs.docker.com/get-docker/)

## Frameworks & Libraries:
- [JUnit 4](https://junit.org/junit4/)
- [Spring Boot Testing module](https://docs.spring.io/spring-boot/docs/2.1.3.RELEASE/reference/html/boot-features-testing.html)
- [Testcontainers MySQL module](https://www.testcontainers.org/modules/databases/mysql/)

## TODO:
- [] Implement database integration test for [UserRepository](src/main/java/com/speedrent/workshop/automation/user/UserRepository.java) with [`@DataJpaTest`](https://docs.spring.io/spring-boot/docs/2.1.3.RELEASE/api/org/springframework/boot/test/autoconfigure/orm/jpa/DataJpaTest.html)
- [] Implement unit tests for [UserService](src/main/java/com/speedrent/workshop/automation/user/UserService.java) with [Mockito](https://site.mockito.org/)
- [] Implement slice tests for [UserController](src/main/java/com/speedrent/workshop/automation/user/UserController.java) and [PostController](src/main/java/com/speedrent/workshop/automation/post/PostController.java) with [`@WebMvcTest`](https://docs.spring.io/spring-boot/docs/2.1.3.RELEASE/api/org/springframework/boot/test/autoconfigure/web/servlet/WebMvcTest.html) and [`@WithMockUser`](https://docs.spring.io/spring-security/site/docs/5.1.4.RELEASE/api/org/springframework/security/test/context/support/WithMockUser.html)
- [] Implement rest client tests for [PostRemoteRepository](src/main/java/com/speedrent/workshop/automation/post/PostRemoteConfiguration.java) with [`@RestClientTest`](https://docs.spring.io/spring-boot/docs/2.1.3.RELEASE/api/org/springframework/boot/test/autoconfigure/web/client/RestClientTest.html)
- [] Implement end-to-end integration tests for [UserController](src/main/java/com/speedrent/workshop/automation/user/UserController.java) and [PostController](src/main/java/com/speedrent/workshop/automation/post/PostController.java) with [`@SpringBootTest`](https://docs.spring.io/spring-boot/docs/2.1.3.RELEASE/api/org/springframework/boot/test/context/SpringBootTest.html)

_*NOTE*: All integration tests should be using [Testcontainers MySQL module](https://www.testcontainers.org/modules/databases/mysql/) as runtime database._
